= Section Headings
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:linkattrs:
// External URIs
:uri-adoc-manual: http://asciidoctor.org/docs/user-manual
:uri-sections: {uri-adoc-manual}/#sections

On this page, you'll learn:

* [x] How to mark up section headings with AsciiDoc.

== Heading level syntax

Sections partition a page into a content hierarchy.
In AsciiDoc, sections are defined using section titles.

.Section title syntax
[source,asciidoc]
----
== Level 1 Section Title

=== Level 2 Section Title

==== Level 3 Section Title

===== Level 4 Section Title

====== Level 5 Section Title (maximum level depth)

== Another Level 1 Section Title
----

When a page is converted to HTML, each section title becomes a heading element where the heading level matches the number of equals signs.
For example, a level 1 section (`==`) maps to an `<h2>` HTML tag.

[discrete]
==== Asciidoctor resources

* {uri-sections}[Section titles^]
