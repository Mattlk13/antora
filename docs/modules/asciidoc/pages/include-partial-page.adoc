= Insert a Partial Page into a Standard Page
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:linkattrs:
// URLs
:uri-adoc-manual: http://asciidoctor.org/docs/user-manual
:uri-include: {uri-adoc-manual}/#include-directive

[#insert-partial]
== Insert a partial

To insert a partial AsciiDoc file into a standard page, use the include directive (`+include::[]+`).

.Insert a partial page using an include directive
[source,asciidoc]
----
\include::{partialsdir}/log-definition.adoc[]
----

Let's break this down.
You start with the include directive prefix, `include::`.
Next is the target.
Start the target with the `+{partialsdir}+` reference to tell Antora where to look for the file.
Then put the relative path of the partial after that.
Finally, end with a pair of square brackets (`+[]+`).

A xref:ROOT:modules.adoc#partials-dir[module's partial AsciiDoc files] should be saved in the [.path]_{blank}_partials_ directory.
You don't need to set the path to this directory in the header of your AsciiDoc file.
When viewing the file in a preview tool, the path is managed by the adjacent [.path]_{blank}_attributes.adoc_ file.

NOTE: Antora doesn't yet support filtering included lines by line number, only by tags.

[discrete]
==== Asciidoctor resources

* {uri-include}[Using the AsciiDoc include directive^]
