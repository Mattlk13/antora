= Comments
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:linkattrs:
// External URIs
:uri-adoc-manual: http://asciidoctor.org/docs/user-manual
:uri-comments: {uri-adoc-manual}/#comments

On this page, you'll learn:

* [x] How write a comment line or block in an AsciiDoc file.

== Comment syntax

You can use a comment line or comment block when you want to add text to an AsciiDoc source file, but don't want that text to be displayed when the file is converted to an HTML page.

A comment line is denoted by two consecutive forward slashes (`//`).

.Comment line syntax
[source,asciidoc]
----
// This is a line that is commented out.
----

A comment block is delimited by a set of four consecutive forward slashes (`////`).

.Comment block syntax
[source,asciidoc]
----
////
This is a comment block.

All of the text, including any AsciiDoc syntax, won't be visible when the file is converted to HTML.
////
----

[discrete]
==== Asciidoctor resources

* {uri-comments}[Comment lines and blocks^]
